Composer Security Checker
=========================

This is a re-implementation of the [SensioLabs Security Checker](https://github.com/sensiolabs/security-checker),
and it's web API ([security.symfony.com](https://security.symfony.com/)) with
100% free and open source code.

It was created after the sunsetting of the free as in beer service that
provided security checks for Composer projects in anticipation of replacing
it with one that imposed strict rate limits unless you paid up.

The requirements for this project are:
* Fully local, any server is optional
* Uses the same [security-advisories](https://github.com/FriendsOfPHP/security-advisories) database
* Minimal dependencies for easy auditing and deployment
* 100% identical to the existing, proprietary service
* Licensed under a strong copyleft license to keep the project free

There are currently two dependencies:
* composer/semver - A component of Composer, required for parsing version
                    constraints.
* symfony/yaml - Part of the Symfony project, required for parsing yaml files.

To set up, copy config.ini.example to config.ini, and update the location
of where the security advisories are located. You probably want to set up a
cron job to automatically git pull that repository.

This project is released under the AGPL, v3 or any later version. See
COPYING for more details.
