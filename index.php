<?php
/**
 * Copyright (C) 2019 Kunal Mehta <legoktm@debian.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Legoktm\SecurityChecker;

require_once __DIR__ . '/vendor/autoload.php';

$conf = parse_ini_file( __DIR__ . '/config.ini' );

$advisories = new Advisories( $conf['advisories'] );

function help( $error = '' ) {
	global $conf, $advisories;
	if ( $error ) {
		$error = htmlspecialchars( $error );
		// TODO: Add the CSS for this
		$error = "<div class=\"error\">$error</div><br />";
	}
	$sha1 = $advisories->getSha1();
	$sha1Link = $advisories->getSha1Link( $sha1 );
	echo <<<HTML
<html>
<head>
	<title>Composer Security Checker</title>
</head>

<body>
	<p>
		{$error}
		Usage: <code>curl -i -H 'Accept: text/plain' https://{$conf['hostname']}/ -F lock=@composer.lock</code>
	</p>

	<p>
		See the <a href="https://gitlab.com/legoktm/composer-security-checker">source code</a>.
		Based off of the <a href="$sha1Link">Friends of PHP security advisories database</a> ($sha1).
	</p>
</body>
</html>
HTML;
}

$accept = [
	'text/plain' => TextFormatter::class,
	'application/json' => JSONFormatter::class,
	'text/markdown' => TextFormatter::class,
	'text/yaml' => YAMLFormatter::class,
];

if ( isset( $accept[$_SERVER['HTTP_ACCEPT']] ) ) {
	$fmtClass = $accept[$_SERVER['HTTP_ACCEPT']];
} else {
	$fmtClass = TextFormatter::class;
}

if ( !isset( $_FILES['lock'] ) ) {
	help();
	exit;
}

$lock = $_FILES['lock']['tmp_name'];
try {
	$checker = new Checker( $lock, $conf['advisories'] );
	$issues = $checker->check();
} catch ( \Exception $e ) {
	help( $e->getMessage() );
	exit;
}

$fmt = new $fmtClass();

header( 'Content-Type: ' . $fmt::TYPE );
header( 'X-Alerts: ' . count( $issues ) );
header( 'X-Database-Version: ' . $advisories->getSha1() );

echo $fmt->format( $issues );
