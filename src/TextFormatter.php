<?php
/**
 * Copyright (C) 2019 Kunal Mehta <legoktm@debian.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Legoktm\SecurityChecker;

class TextFormatter {

	const TYPE = 'text/plain';

	private $missingCve = 1;

	private function underline( $str, $char = '-' ) {
		return str_repeat( $char, strlen( $str ) );
	}

	private function nextMissingCve() {
		return sprintf( 'CVE-NONE-%04d', $this->missingCve++ );
	}

	public function format( array $issues ) {
		$text = $heading = 'Composer Security Check Report';
		$text .= "\n" . $this->underline( $heading, '=' );
		$text .= "\n\n";
		$count = count( $issues );
		if ( $count === 0 ) {
			$text .= "No packages have known vulnerabilities.\n\n";
			return $text;
		}
		// FIXME: plurals
		$text .= "$count packages have known vulnerabilities.\n\n";
		$links = [];
		foreach ( $issues as $name => $info ) {
			$section = $header = "$name ({$info['version']})";
			$section .= "\n" . $this->underline( $header ) . "\n\n";
			foreach ( $info['advisories'] as $adv ) {
				if ( $adv['cve'] ) {
					$cve = $adv['cve'];
				} else {
					$cve = $this->nextMissingCve();
				}
				$links[$cve] = $adv['link'];
				$section .= " * [$cve][]: {$adv['title']}\n";
			}
			$section .= "\n";
			$text .= $section;
		}
		foreach ( $links as $cve => $link ) {
			$text .= "[$cve]: $link\n";
		}
		$text .= "\n";

		$text .= <<<DISCLAIMER
Note that this checker can only detect vulnerabilities that are referenced in the SensioLabs security advisories database.
Execute this command regularly to check the newly discovered vulnerabilities.
DISCLAIMER;
		return $text;
	}
}
