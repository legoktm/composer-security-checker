<?php
/**
 * Copyright (C) 2019 Kunal Mehta <legoktm@debian.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Legoktm\SecurityChecker;

use Symfony\Component\Yaml\Yaml;

class YAMLFormatter implements Formatter {

	const TYPE = 'text/yaml';

	public function format( array $issues ) {
		// TODO: This is not 100% identical to the service output
		return Yaml::dump( $issues, 3 );
	}
}
