<?php
/**
 * Copyright (C) 2019 Kunal Mehta <legoktm@debian.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Legoktm\SecurityChecker;

use Composer\Semver\VersionParser;
use Composer\Semver\Constraint\Constraint;
use Symfony\Component\Yaml\Yaml;

class Checker {

	public function __construct( $lock, $advisories ) {
		$this->lockFile = $lock;
		$this->lock = json_decode( file_get_contents( $lock ), true );
		if ( !is_array( $this->lock ) || !isset( $this->lock['packages'] ) ) {
			throw new \Exception( 'Invalid composer.lock' );
		}
		$this->advisories = $advisories;
		$this->versionParser = new VersionParser();
	}

	private function isValidName( $name ) {
		// https://getcomposer.org/doc/04-schema.md#name claims that
		// any character can be used, but that seems way too permissive.
		// For now we allow alphanumeric and - (dash).
		return preg_match( '%[A-z0-9\-]/[A-z0-9\-]%', $name ) === 1;
	}

	/**
	 * Strip a leading "v" from the version name
	 *
	 * @param string $version
	 * @return string
	 */
	public function normalizeVersion( $version ) {
		if ( strpos( $version, 'v' ) === 0 ) {
			// Composer auto-strips the "v" in front of the tag name
			$version = ltrim( $version, 'v' );
		}
		return $version;
	}

	public function check() {
		$issues = [];
		foreach ( $this->getInstalled() as $name => $version ) {
			$found = $this->findVulnerabilities( $name, $version );
			if ( $found ) {
				$advisories = [];
				foreach ( $found as $adv ) {
					$advisories[] = [
						'title' => $adv['title'],
						'link' => $adv['link'],
						'cve' => isset( $adv['cve'] ) ? $adv['cve'] : '',
					];
				}
				$issues[$name] = [
					'version' => $version,
					'advisories' => $advisories,
				];
			}
		}

		return $issues;
	}

	public function getInstalled() {
		// Flatten lock file
		$deps = [];
		foreach ( $this->lock['packages'] as $pkg ) {
			if ( !$this->isValidName( $pkg['name'] ) ) {
				throw new \Exception( "Invalid package name: {$pkg['name']}" );
			}
			$deps[$pkg['name']] = $this->normalizeVersion( $pkg['version'] );
		}

		return $deps;
	}

	public function findVulnerabilities( $name, $version ) {
		// This seems sketch but we validated the package name
		// in getInstalled()
		if ( !is_dir( "{$this->advisories}/$name" ) ) {
			return [];
		}

		$issues = [];
		$installed = new Constraint( '==', $this->versionParser->normalize( $version ) );
		$vulns = glob( "{$this->advisories}/$name/*.yaml" );
		sort( $vulns );
		foreach ( $vulns as $vuln ) {
			$info = Yaml::parseFile( $vuln );
			foreach ( $info['branches'] as $branch => $binfo ) {
				$vulnerable = implode( ',', $binfo['versions'] );
				if ( $this->versionParser->parseConstraints( $vulnerable )->matches( $installed ) ) {
					// !! vulnerable :(
					$issues[] = $info;
					break;
				}
			}
		}

		return $issues;
	}
}
