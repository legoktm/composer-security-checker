#!/usr/bin/env php
<?php
/**
 * Copyright (C) 2019 Kunal Mehta <legoktm@debian.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Legoktm\SecurityChecker;

require_once __DIR__ . '/vendor/autoload.php';

function help() {
	echo <<<HELP
Usage:

./check.php composer.lock [security-advisories]

The location of the security-advisories repository
can also be provided as an environment variable:
PHP_SECURITY_ADIVSORIES=/path/to/security-advisories
HELP;
	exit(1);
}

if ( !isset( $argv[1] ) ) {
	help();
}

$lock = $argv[1];

$advisories = false;
if ( isset( $argv[2] ) ) {
	$advisories = $argv[2];
} elseif ( getenv( 'PHP_SECURITY_ADIVSORIES' ) ) {
	$advisories = getenv( 'PHP_SECURITY_ADIVSORIES' );
}

if ( $advisories === false || !is_dir( $advisories ) ) {
	help();
}

$checker = new Checker( $lock, $advisories );
$issues = $checker->check();
$fmt = new YAMLFormatter();
echo $fmt->format( $issues );
exit( $issues ? 1 : 0 );
